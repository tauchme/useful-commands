# Useful commands
This project contains a collection of custom console commands, each one implemented as a separate file.
They are written in Ruby so it **is** a necessary dependency. <sub><sup>(It comes preinstalled with MacOS 
btw)</sup></sub>

To understand the functionality of each command, refer to the `description` method at the top of each file.

## How to install
###### I use MacOS although for Linux should be similar.
### As super user so anyone can invoke the command:
- Login as superuser
```
sudo su
```
- Navigate to ```/usr/local/bin/```
- Paste here the command you want to have available.

### For individual user (Using ZSH):
- Create a folder called `bin` or `.bin` at your home path.
- Go to your `.zshrc` file and uncomment the following line:
```
# export PATH=$HOME/bin:/usr/local/bin:$PATH
```
- Remember to change `$HOME/bin` to `$HOME/.bin` if you created a hidden `bin` folder.
- Place all the files you want from this repo inside the `bin` folder.
